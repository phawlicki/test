# NumberToBinaryConverter
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Examples](#setup)
* [Author](#author)


## General info
This is microservice app which can convert BigInteger to binary notation and compare number of occurences 0 and 1.
This microservice contains from:

- Discovery server
- Config server
- BinaryConverter server

	
## Technologies
Project is created with:
* Spring Boot
* Spring Cloud
* Java 1.8
	
## Setup
To run this project execute:

* git clone --recursive https://gitlab.com/phawlicki/test.git

To run this app execute:

- mvn clean package in discoveryserver directory
- mvn clean package in configserver directory
- mvn clean package in binarycalculator directory

Afterwards go to main directory and execute
docker-compose up


- Eureka discovery server will run on port 9091
- Config server will run on port 9090
- BinaryConverter server will run on port 8085

## Examples
* To test a number simply use this pattern

http://localhost:8085/api/test/{PRIME}
PRIME is a BigInteger number

## Author
* Przemysław Hawlicki